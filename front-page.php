<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Jenessa Seruela Real Estate
 * @subpackage JenessaSeruelaRealEstate
 * @since 1.0
 * @version 1.0
 */

get_header(); 

// Display About Section.
get_template_part( 'template-parts/section/section', 'about' );
?>
<div class="spacer"></div>
<?php
// Display Properties Section.
get_template_part( 'template-parts/section/section', 'properties' );
?>
<div class="spacer"></div>
<?php
// Display Accreditation Section.
get_template_part( 'template-parts/section/section', 'accreditation' );
?>
<div class="spacer"></div>
<?php
// Display Contact Us Section.
get_template_part( 'template-parts/section/section', 'contact' );

get_footer(); 
?>   