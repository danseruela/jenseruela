<?php
/**
 * The template for displaying the footer
 *
 * @package CEA Creatives
 * @subpackage CEACreative
 * @since 1.0
 * @version 1.0
 */
?>

<footer class="card-deck">
    
      <?php get_template_part( 'template-parts/footer/footer', 'widgets' ) ?>    
    
    <!-- <a href="javascript:" id="return-to-top">
      <i class="fa fa-chevron-up"></i>
    </a> -->
  
      <?php get_template_part( 'template-parts/footer/site', 'info' ) ?>
</footer>

<!-- messenger app -->
<!-- add here -->

<?php wp_footer(); ?>
  
</body>
</html>