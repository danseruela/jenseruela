<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WP Bootstrap
 * @subpackage SimplyBootstrap
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="container" style="margin-top:4rem !important;">
	<div id="primary" class="row">
		
			<div class="col-sm-8">
			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();
			?>
			<header>
				<div class="post-thumbnail">
					<a href="<?php the_permalink(); ?>">
						<?php the_post_thumbnail( 'full', array( 'class' => 'img-fluid' ) ); ?>
					</a>
				</div><!-- .post-thumbnail -->
			</header>	
			<div class="my-4">
			<?php
				//get_template_part( 'template-parts/post/content', get_post_format() );
				if ( is_page() ) {
					the_title( $before = '<h1 class="mt-4 mb-1">', $after = '</h1>' );
				}
			?>
				<div class="entry-content my-3">
					<?php
					/* translators: %s: Name of current post */
					the_content( sprintf(
						__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'simplybootstrap' ),
						get_the_title()
					) );

					wp_link_pages( array(
						'before'      => '<div class="page-links">' . __( 'Pages:', 'simplybootstrap' ),
						'after'       => '</div>',
						'link_before' => '<span class="page-number">',
						'link_after'  => '</span>',
					) );

					the_post_navigation( array(
						'prev_text' 			=> '<span>' . __( '', 'simplybootstrap' ) . '<span class="nav-title"><span class="nav-title-icon-wrapper"><i class="fa fa-arrow-left"></i> </span>%title</span>',
						'next_text' 			=> '<span>' . __( '', 'simplybootstrap' ) . '<span class="nav-title">%title<span class="nav-title-icon-wrapper"> <i class="fa fa-arrow-right"></i></span></span>',
						'in_same_term'			=>	true,
						'screen_reader_text'	=>	__( 'Continue Reading' ),
					) );
					?>
				</div><!-- .entry-content -->
				<hr>
		<?php endwhile; ?> <!-- // End of the loop. -->

			</div><!-- .my-5 -->
			</div><!-- .col-sm-8 -->
			<div class="col-sm-4">
				<?php get_sidebar(); ?>
			</div>
			

	</div><!-- #row -->
	</div><!-- .container -->

</article>	
<?php 
	// Display CTA section
    get_template_part( 'template-parts/section/section', 'cta' );
	get_footer();
?>	
