<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Jenessa Seruela Real Estate
 * @subpackage JenessaSeruelaRealEstate
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
	<header>
		<div class="header-image-post" style="background-image: url('<?php echo get_template_directory_uri() ?>/img/bg-1600x867.jpg');">
			<div class="header-caption container text-center d-none d-md-block">
	        </div>
		</div>	
	</header>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div id="single" class="card-deck">
			<div class="col-sm-12">
				<?php
				/* Start the Loop */
				while ( have_posts() ) : the_post();

				// Get categories assigned to a post.
				$taxonomy = 'category';
		
				// Get the term IDs assigned to post.
				$post_terms = wp_get_object_terms( $post->ID, $taxonomy, array( 'fields' => 'ids' ) );
				
				// Separator between links.
				$separator = ', ';
				
				if ( ! empty( $post_terms ) && ! is_wp_error( $post_terms ) ) {
				
					$term_ids = implode( ',' , $post_terms );
				
					$terms = wp_list_categories( array(
						'title_li' => '',
						'style'    => 'list',
						'echo'     => false,
						'include'  => $term_ids,
					) );
				
					$terms = rtrim( trim( str_replace( '<br />',  $separator, $terms ) ), $separator );
				}
				?>
				<?php 
				//if it's not a specific post-type == listing
				if ( is_single() && get_post_type() != 'listings' ) { 
				?>
					<header>
						<div class="post-thumbnail text-center">
							<a href="<?php the_permalink(); ?>">
								<?php the_post_thumbnail( 'full', array( 'class' => 'img-fluid' ) ); ?>
							</a>
						</div><!-- .post-thumbnail -->
					</header>	
				<?php } ?>
				<div class="my-4">
				<?php
					//if it's a specific post-type = listing
					if ( is_single() && get_post_type() == 'listings' ) {
						the_title( $before = '<h1 class="mt-4 mb-1">', $after = '</h1>' );
					}
				?>
					<div>
						<span style="font-size: 13px;">
							Date posted: <?php the_date(); ?><br/>
							<!-- Category: <?php // echo $terms ?> -->
						</span>
					</div>
					<div class="entry-content my-3">
						<div class="row">
							<div class="col-sm-12">
								<?php  the_content(); ?>
							</div>
						</div>
						
						<!-- Interested Section -->
						<div class="listing-details-contact">
							<div class="container">
								<div class="row">
									<div class="col-md-4">
										<div class="list-details-contact-photo">
											<canvas width="320" height="290" style="background-image: url('<?php echo get_template_directory_uri() ?>/img/about/about-page-jenessa-seruela.jpg')"></canvas>
										</div>
										<canva></canva>
									</div>
									<div class="col-md-8">
										<div class="listing-contact-item-top">
											<div class="list-details-contact-agent">
												<strong>Interested in this Listing?</strong>
											</div>
											<div class="list-details-contact-name">
												<span>Contact</span>
												Jenessa Seruela
											</div>
										</div>
										<div class="listing-contact-mid-holder">
											<div class="listing-contact-item">
												<span class="fa fa-phone"></span>
												<a class="telno" href="tel:+63.933.016.7011">+63.933.016.7011</a>
											</div>
											<div class="listing-contact-item">
												<span class="fa fa-envelope"></span>
												<a class="emailto" href="mailto:info@jenseruela.com">Send Email</a>
											</div>
											<div class="list-contact-btn">
												<a href="#contact" class="btn btn-dark">
													<span>Get In Touch</span>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<!-- Recent Posts Section -->
						<h5 class="text-center mt-5">Recent Listings</h5>
						<div id="recent-post" class="row mt-5 mb-5">
						<?php 
						$args = [ 
							'post_type'         =>  'listings',
							'posts_per_page'    => 	4 
						];

						// Define our WP Query Parameters
						$recent_post = new WP_Query( $args ); ?>
						
						
						<?php 
						// Start our WP Query
						while ($recent_post -> have_posts()) : $recent_post -> the_post(); 
						// Display the Post Title with Hyperlink
						?>
						
						
						<div id="recent-post" class="listing col-sm-3 col-pad-2">
							<div class="card">
								<img class="card-img-top" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
								<div class="card-body">
									<h6 class="card-title"><?php the_title(); ?></h6>
									<p>
										<?php echo get_post_meta( get_the_ID(), 'location', true); ?><br/>
										<b><?php echo get_post_meta( get_the_ID(), 'price', true); ?></b>
									</p>
									<div class="text-center">
										<a class="btn btn-dark" href="<?php the_permalink(); ?>">View Details</a>
									</div>
								</div>
							</div>
						</div>
						
						
						<?php 
						// Repeat the process and reset once it hits the limit
						endwhile;
						wp_reset_postdata();
						?>
						</div>

						<?php

						// wp_link_pages( array(
						// 	'before'      => '<div class="page-links">' . __( 'Pages:', 'jenseruela' ),
						// 	'after'       => '</div>',
						// 	'link_before' => '<span class="page-number">',
						// 	'link_after'  => '</span>',
						// ) );

						// the_post_navigation( array(
						// 	'prev_text' 			=> '<span>' . __( '', 'jenseruela' ) . '<span class="nav-title"><span class="nav-title-icon-wrapper"><i class="fa fa-arrow-left"></i> </span>%title</span>',
						// 	'next_text' 			=> '<span>' . __( '', 'jenseruela' ) . '<span class="nav-title">%title<span class="nav-title-icon-wrapper"> <i class="fa fa-arrow-right"></i></span></span>',
						// 	'in_same_term'			=>	true,
						// 	'screen_reader_text'	=>	__( 'More Listings' ),
						// ) );
						?>
					</div><!-- .entry-content -->
					<!-- Comments Form -->
					<?php 
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
					
				endwhile; // End of the loop.
				?>
				</div><!-- .my-4 -->
			</div><!-- .col-sm-12 -->		
		</div><!-- #row -->

	</article>	
<?php 
	// Display Contact Us Section.
	get_template_part( 'template-parts/section/section', 'contact' );

	// Display Footer
	get_footer();
?>	
