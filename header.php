<?php
/**
 * The template for displaying the header
 *
 * @package Jenessa Seruela Real Estate
 * @subpackage JenessaSeruelaRealEstate
 * @since 1.0
 * @version 1.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Jenessa is an accountant of an ISO certified company in MEPZ, Lapu-Lapu City with a passion for real estate. She views it is a true privilege to provide clients guidance with their home buying and selling.">
    <meta name="author" content="Dan Laurice Seruela">
    <meta name="keywords" content="Jenessa Seruela Real Estate, real estate, jenessaseruela, jen seruela, jen pesquera, jenessa pesquera seruela, real estate sales agent, real estate agent">
    <title>
      <?php 
          if (function_exists('is_tag') && is_tag()) { 
            echo 'Tag Archive for &quot;'.$tag.'&quot; - '; 
          } elseif (is_archive()) { 
            wp_title(''); echo ' Archive - '; 
          } elseif (is_search()) { 
            echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; 
          } elseif (is_home() || is_front_page()) {
            bloginfo('name'); 
          } elseif (!(is_404()) && (is_single()) || (is_page())) { 
            wp_title('|', true, 'right'); //echo ' - '; 
          } elseif (is_404()) {
            echo 'Not Found - '; 
          } else {
            bloginfo('name'); 
          }
      ?>
    </title>

    <!-- Bootstrap core CSS -->
    <?php wp_head(); ?>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    
</head>

<body <?php body_class('scrollbar-default square'); ?>>
<?php 

    // Display header menus.
    get_template_part( 'template-parts/header/header', 'menu' );

    if( is_home() || is_front_page() ) {
      // Display header carousel.
      get_template_part( 'template-parts/header/header', 'carousel' ); 
    } 
?>
