(function($) {

$(document).ready(function(){
	// === Add class for search field
	// $('.search-field').addClass('form-control');
	// $('.search-submit').addClass('btn btn-dark');


	// ===== Same page anchor link effects
	$('.menu-item a, #getquote').click(function(e) {
		e.preventDefault();
		var aid = $(this).attr("href");
		
		// Check if link is of the same page
    	if (this.pathname == window.location.pathname &&
		    this.protocol == window.location.protocol &&
			this.host == window.location.host && 
			aid.includes("#")) {
			$('html,body').animate({
	    		scrollTop: $(aid).offset().top - 100 // - 90
			}, 100);
			// Test
			console.log("Pathname: " + this.pathname + " = " + window.location.pathname);
			console.log("Protocol: " + this.protocol + " = " + window.location.protocol);
			console.log("Host: " + this.host + " = " + window.location.host);
			console.log(aid);
		} else {
			console.log(aid);
			window.location.href = aid;
		}
	});

	// ===== Sticky navbar when scrolled down =====
	$(window).scroll(function() {
		var scrollPos = $(window).scrollTop();
		var windowWidth = $(window).width();

		if ( scrollPos >= 30 ) {
			$('#navbar-main').removeClass('bg-light');
			$('#navbar-main').addClass('bg-scrolled');
			$('.navbar .menu-item a').css('color', '#000');
			$('.navbar .menu-item a').hover(function() {
				$(this).css('color' , '#800000');
			}, function() {
				$(this).css('color' , '#000');
			});
			$('.navbar-brand img').attr({
				src: window.location.origin + '/wp-content/themes/jenseruela/img/logo/js-real-estate-logo-300x67.png',
			});
			if( windowWidth <= 425 && windowWidth > 375 ) { /* Mobile - L */
				$('.navbar-brand img').css({
					width: '300px',
					transition: 'all 0.3s ease'
				});
				$('#navbar-main').css({ background: '#800000' });
				$('.navbar .menu-item a').hover(function() {
					$(this).css('color' , '#e1e1e1');
				}, function() {
					$(this).css('color' , '#000');
				});
			} else if( windowWidth <= 375 && windowWidth > 320 ) { /* Mobile - M */
				$('.navbar-brand img').css({
					width: '258px',
					transition: 'all 0.3s ease'
				});
				$('#navbar-main').css({ background: '#800000' });
				$('.navbar .menu-item a').hover(function() {
					$(this).css('color' , '#e1e1e1');
				}, function() {
					$(this).css('color' , '#000');
				});
			} else if( windowWidth <= 320 ) { /* Mobile - S */
				$('.navbar-brand img').css({
					width: '202px',
					transition: 'all 0.3s ease'
				});
				$('#navbar-main').css({ background: '#800000' });
				$('.navbar .menu-item a').hover(function() {
					$(this).css('color' , '#e1e1e1');
				}, function() {
					$(this).css('color' , '#000');
				});
			} else {
				$('.navbar-brand img').css({
					width: '300px',
					transition: 'all 0.3s ease'
				});
			}	
		} else {
			$('#navbar-main').removeClass('bg-scrolled');
			$('.navbar .menu-item a').css('color', '#fff');
			$('.navbar .menu-item a').hover(function() {
				$(this).css('color' , '#800000');
			}, function() {
				$(this).css('color' , '#fff');
			});
			$('.navbar-brand img').attr({
				src: window.location.origin + '/wp-content/themes/jenseruela/img/logo/js-real-estate-white-logo-300x67.png',
			});		
			if( windowWidth <= 425 && windowWidth > 375 ) { /* Mobile - L */
				$('.navbar-brand img').css({
					width: '300px',
				});
				$('#navbar-main').css({ background: '#800000' });
				$('.navbar .menu-item a').hover(function() {
					$(this).css('color' , '#e1e1e1');
				}, function() {
					$(this).css('color' , '#fff');
				});
			} else if( windowWidth <= 375 && windowWidth > 320 ) { /* Mobile - M */
				$('.navbar-brand img').css({
					width: '258px',
				});
				$('.navbar .menu-item a').hover(function() {
					$(this).css('color' , '#e1e1e1');
				}, function() {
					$(this).css('color' , '#fff');
				});
			} else if( windowWidth <= 320 ) { /* Mobile - S */
				$('.navbar-brand img').css({
					width: '202px',
				});
				$('.navbar .menu-item a').hover(function() {
					$(this).css('color' , '#e1e1e1');
				}, function() {
					$(this).css('color' , '#fff');
				});
			} else {
				$('.navbar-brand img').css({
					width: '300px',
				});
			}		
		}
	});

	// Add attrib for navbar main menu links in mobile view
	function mediaSize() {
		if(window.matchMedia('(max-width: 425px)').matches) {
			$('#navbar-main #menu-main-menu li a').attr({
				'data-toggle' : 'collapse',
				'data-target' : '#navbarSupportedContent'
			});
		} else {
			$('#navbar-main #menu-main-menu li a').removeAttr('data-toggle data-target');
		}
	}
	// call function
	mediaSize();

	// Attach the function to the resize event listener
	window.addEventListener('resize', mediaSize, false);

});

})(jQuery);
	