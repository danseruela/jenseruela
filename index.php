<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 *
 * @package CEA Creatives
 * @subpackage CEACreative
 * @since 1.0
 * @version 1.0
 */
 
 get_header(); ?>

  <div class="container">
      <?php if ( is_home() && ! is_front_page() ) : ?>
        <header class="page-header" style="margin-top: 7rem;">
          <h1 class="page-title"><?php single_post_title(); ?></h1>
        </header>
      <?php else : ?>
        <header class="page-header" style="margin-top: 7rem;">
          <h2 class="page-title"><?php _e( 'Posts', 'ceacreative' ); ?></h2>
        </header>
      <?php endif; ?>   

      <div class="row">
          <div class="my-4">
          <div class="col-sm-8">
            <?php
              if ( have_posts() ) :

                /* Start the Loop */
                while ( have_posts() ) : the_post();

                  /*
                   * Include the Post-Format-specific template for the content.
                   * If you want to override this in a child theme, then include a file
                   * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                   */
                  //get_template_part( 'template-parts/post/content', get_post_format() );
                  echo 'Display Archive Post Here...';

                endwhile;

                the_posts_pagination( array(
                  'prev_text' => '<i class="fa fa-arrow-left"></i>' . '<span class="screen-reader-text">' . __( 'Previous page', 'simplybootstrap' ) . '</span>',
                  'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'simplybootstrap' ) . '</span>' . '<i class="fa fa-arrow-right"></i>',
                  'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'simplybootstrap' ) . ' </span>',
                ) );

              else :

                echo 'Nothing Found!';

              endif;
            ?>
          </div><!-- .col-sm-8 -->
          </div><!-- .my-4 -->
      </div><!-- .row -->
  </div><!-- .container -->

<?php get_footer(); ?>
