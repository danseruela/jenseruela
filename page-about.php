<?php
/**
 * Template Name: About Page Template
 *
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Jenessa Seruela Real Estate
 * @subpackage JenessaSeruelaRealEstate
 * @since 1.0
 * @version 1.0
 */

get_header(); 

?>
	<header>
		<div class="header-image-post" style="background-image: url('<?php echo get_template_directory_uri() ?>/img/bg-1600x867.jpg');">
			<div class="header-caption container text-center d-none d-md-block">
	        </div>
		</div>	
	</header>
	

	<div id="about">
		<div class="aboutLeft text-center">
			<img class="img-fluid" style="width: 600px; margin-top: 4rem;" src="<?php echo get_template_directory_uri(); ?>/img/about/about-page-jenessa-seruela.jpg" alt="About Jenessa Seruela"><br/>
			<img class="img-fluid" style="width: 600px;" src="<?php echo get_template_directory_uri(); ?>/img/about/accreditations.jpg" alt="Accreditations">
		</div>
		<div class="aboutRight">
			<span class="subheader"><b>ABOUT</b></span>
			<h1 class="aboutH1">JENESSA <b>SERUELA</b></h1>
			<p><b>PRC & DSHSUD Accredited Real Estate Salesperson</b></p>
			<p>Jenessa is a trustworthy and reliable accountant of an ISO certified company in MEPZ, Lapu-Lapu City with a passion for real estate. 
				Her top priority is to  provide clients need & guidance with their home buying & selling for the right price under the best terms. 
				Jenessa dedicates her time & efforts to delivering clients white-glove service & strives for best results. 
				She earns the respect of her clients by working diligently on their behalf & by always offering them candid advice with warm & 
				friendly approach.  Moreover, she will make sure to perform the highest levels of honesty, integrity & professionalism with gratitude.
			</p>
			<p>Raised in the Island of Pilar, a small island of Camotes, Cebu.
				She understands the economic status of our country and job status of her prospects, which has helped her to understand her clients’ needs and 
				provide them with the best service possible. Jenessa’s background in multi-level marketing and project management 
				have also served as one of the foundations for her success as a real estate agent.
			</p>
			<p>While at Compass, Jenessa has played an integral role in overseeing multimillion dollar real estate transactions. 
				Always aiming to exceed expectations, Jenessa is committed to serving her clients.
			</p>
		</div>
	</div>

	<div class="spacer"></div>

<?php
	// Display Contact Us Section.
	get_template_part( 'template-parts/section/section', 'contact' );

	// Display Footer
	get_footer();
?>	
