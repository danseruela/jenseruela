<?php
/**
 * Template Name: Listings Archive Template
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Jenessa Seruela Real Estate
 * @subpackage JenessaSeruelaRealEstate
 * @since 1.0
 * @version 1.0
 */

get_header(); 

$args = [ 
    'post_type'         =>  'listings',
    'posts_per_page'    => 20 
];
$properties = new WP_Query( $args );

?>
<header>
    <div class="header-image-post" style="background-image: url('<?php echo get_template_directory_uri() ?>/img/bg-1600x867.jpg');">
        <div class="header-caption container text-center d-none d-md-block">
        </div>
    </div>	
</header>

<div id="properties-page" class="card-deck">
    <h1 class="text-center">PROPERTIES</h1><br>
    <p class="text-center text-muted">
        <!-- Search Form -->
        <?php get_search_form(); ?>
    </p>
    
    <!-- Contents of Services by Cards -->
    <?php if( $properties->have_posts() ) { ?>
    <div class="row mt-5 mb-5">
    <?php    

        while ( $properties->have_posts() ) : $properties->the_post(); 

        // Get categories assigned to a post.
        $taxonomy = 'category';

        // Get the term IDs assigned to post.
        $post_terms = wp_get_object_terms( $post->ID, $taxonomy, array( 'fields' => 'ids' ) );
            
        // Separator between links.
        $separator = ', ';
            
        if ( ! empty( $post_terms ) && ! is_wp_error( $post_terms ) ) {
            
            $term_ids = implode( ',' , $post_terms );
            
            $terms = wp_list_categories( array(
                'title_li' => '',
                'style'    => 'list',
                'echo'     => false,
                'taxonomy' => $taxonomy,
                'include'  => $term_ids,
            ) );
            
            $terms = rtrim( trim( str_replace( '<br />',  $separator, $terms ) ), $separator );
        }
    
    ?>
        <div class="listing col-sm-4 col-pad-2">
            <div class="card">
                <img class="card-img-top" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
                <div class="card-body">
                    <h6 class="card-title"><?php the_title(); ?></h6>
                    <p>
                        <?php echo get_post_meta( get_the_ID(), 'location', true); ?><br/>
                        <b><?php echo get_post_meta( get_the_ID(), 'price', true); ?></b>
                    </p>
                    <div class="text-center">
                        <a class="btn btn-dark" href="<?php the_permalink(); ?>">View Details</a>
                    </div>
                </div>
            </div>
        </div>
    
    <?php endwhile; ?>

    </div> <!-- end of .row -->
    <?php } 
    
    // Display Contact Us Section.
    get_template_part( 'template-parts/section/section', 'contact' );

    ?>
    
</div>

<?php get_footer(); ?>