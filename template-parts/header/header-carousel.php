<?php
/**
 * Displays header carousel if assigned
 *
 * @package Jenessa Seruela Real Estate
 * @subpackage JenessaSeruelaRealEstate
 * @since 1.0
 * @version 1.0
 */

?>
<header>
    <div id="carouselHeaderControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="<?php echo get_template_directory_uri(); ?>/img/bg-1600x867.jpg" class="d-block w-100" alt="...">
                <div class="carousel-caption d-md-block has-effects">
                    <span class="subheader"><b>READY TO FIND</b></span>
                    <h1>YOUR DREAM HOME?</h1>
                    <p></p>
                    <a class="btn btn-dark btn-lg text-uppercase" href="#properties">Let's Get Started</a>
                </div>
            </div>
        </div>
    </div>
</header>