<?php
/**
 * Displays footer widgets if assigned
 *
 * @package CEA Creatives
 * @subpackage CEACreative
 * @since 1.0
 * @version 1.0
 */

?>

<?php
if ( is_active_sidebar( 'sidebar-2' ) ||
	 is_active_sidebar( 'sidebar-3' ) ||
	 is_active_sidebar( 'sidebar-4' ) ) :
?>

	
		<?php
		if ( is_active_sidebar( 'sidebar-2' ) ) { ?>
			<div class="col-md-4 my-3" style="padding-left: 0;">
				<?php dynamic_sidebar( 'sidebar-2' ); ?>
			</div>
		<?php }
		if ( is_active_sidebar( 'sidebar-3' ) ) { ?>
			<div class="col-md-4 my-3">
				<?php dynamic_sidebar( 'sidebar-3' ); ?>
			</div>
		<?php }
		if ( is_active_sidebar( 'sidebar-4' ) ) { ?>
			<div class="col-md-4 my-3">
				<?php dynamic_sidebar( 'sidebar-4' ); ?>
			</div>		
		<?php } ?>
	

<?php endif; ?>
