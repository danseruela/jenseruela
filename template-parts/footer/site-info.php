<?php
/**
 * Displays footer site info
 *
 * @package Jenessa Seruela Real Estate
 * @subpackage JenessaSeruelaRealEstate
 * @since 1.0
 * @version 1.0
 */

?>
<div class="copyright text-center">	
	Copyright © 2021 <a href="/">Jenessa Seruela</a>. All Rights Reserved.
	<!-- Maintained by <a href="<?php //echo esc_url( __( '#', 'JenSeruela' ) ); ?>"><?php //printf( __( ' %s', 'JenSeruela' ), 'djtechsolutions.com' ); ?></a> -->
</div><!-- .site-info -->
