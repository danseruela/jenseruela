<?php
/**
 * The template for displaying the About Section
 *
 * @package Jenessa Seruela Real Estate
 * @subpackage JenessaSeruelaRealEstate
 * @since 1.0
 * @version 1.0
 */
?>

<div id="accreditation" class="text-center">
    <!-- <h1 class="text-center">MEMBER & ACCREDITED BY</h1> -->
    <div class="subheader text-center" style="margin: 2rem auto 0;"><b>MEMBER & ACCREDITED BY:</b></div>
    <div class="spacer"></div>
    <p><img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/about/accreditation.jpg" alt="Member and Accredications"></p>
</div>