<?php
/**
 * The template for displaying the Portfolio Section
 *
 * @package Jenessa Seruela Real Estate
 * @subpackage JenessaSeruelaRealEstate
 * @since 1.0
 * @version 1.0
 */
?>
<section id="properties" class="card-deck properties text-center">
    <span class="subheader" style="margin: 2rem auto 0;"><b>FEATURED</b></span>
    <h1>PROPERTIES</h1>
</section>
<div id="properties-section" class="card-deck">
    <!-- <div id="carouselPropertiesControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner"> -->
    <?php 
        $args = [ 
                'post_type'         =>  'listings', 
                'posts_per_page'    =>  10,
                'category_name'     =>  'Featured',
                
        ];
        $the_query = new WP_Query( $args ); 
    ?>
    <?php 
        if ( $the_query->have_posts() ) : 
    ?>
        <div class="row mt-5">
    <?php
            while ( $the_query->have_posts() ) : $the_query->the_post(); 
    ?>
        <div class="featured-listing col-sm-4 col-pad-2">
            <div class="card">
                <img class="card-img-top" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
                <div class="card-body">
                    <h6 class="card-title"><?php the_title(); ?></h6>
                    <p>
                        <?php echo get_post_meta( get_the_ID(), 'location', true); ?><br/>
                        <b><?php echo get_post_meta( get_the_ID(), 'price', true); ?></b>
                    </p>
                    <div class="text-center">
                        <a class="btn btn-dark" href="<?php the_permalink(); ?>">View Details</a>
                    </div>
                </div>
            </div>
        </div>
    <?php 
            endwhile;
            wp_reset_postdata(); 
    ?>
    <?php else: ?>
            <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
    <?php endif; ?>
        </div> <!-- end of .row -->
    <div class="text-center" style="margin: 2rem auto 0;">
        <a href="/listings/" class="btn btn-dark">View More Listings</a>
    </div>
</div>
<div class="spacer"></div>

