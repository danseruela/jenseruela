<?php
/**
 * The template for displaying the Contact Section
 *
 * @package CEA Creatives
 * @subpackage CEACreative
 * @since 1.0
 * @version 1.0
 */
?>
<div id="contact" class="card-deck">
    <div class="subheader text-center" style="margin: 2rem auto 0;"><b>CONTACT</b></div>
    <h1 class="text-center">GET IN TOUCH</h1>
    <div class="contactLeft">
        <?php
        // Display Contact Us Form using shortcode.
        echo do_shortcode( '[contact-form-7 id="17" title="Contact Form"]' );
        ?>
    </div>
    <div class="contactRight">
        <p><i class="fas fa-user-alt"></i> JENESSA P. SERUELA</p>
        <p><i class="fas fa-map-marker-alt"></i> Deca Homes 4, Bankal, Lapu-Lapu City, Cebu</p>
        <p><i class="fas fa-mobile-alt"></i> +63.933.016.7011</p>
        <p><i class="fas fa-envelope"></i> info@jenseruela.com</p>
        <p><i class="fab fa-facebook-square"></i> <a href="https://www.facebook.com/jenessa.pesquera" target="_blank"><b>jenessa.pesquera</b></a></p>
        <p><i class="fab fa-instagram"></i> <a href="https://www.instagram.com/jenessa.seruela" target="_blank"><b>jenessa.seruela</b></a></p>
        <div class="spacer"></div>
    </div>    
</div>