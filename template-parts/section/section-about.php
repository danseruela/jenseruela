<?php
/**
 * The template for displaying the About Section
 *
 * @package Jenessa Seruela Real Estate
 * @subpackage JenessaSeruelaRealEstate
 * @since 1.0
 * @version 1.0
 */
?>
<div id="about">
    <div class="aboutLeft text-center">
        <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/about/about-pic.jpg" alt="About Jenessa Seruela">
    </div>
    <div class="aboutRight">
        <span class="subheader"><b>ABOUT</b></span>
        <h1 class="aboutH1">JENESSA <b>SERUELA</b></h1>
        <p><b>PRC & DSHSUD Accredited Real Estate Salesperson</b></p>
        <p>Jenessa is a trustworthy and reliable accountant of an ISO certified company in MEPZ, Lapu-Lapu City with a passion for real estate. 
            Her top priority is to  provide clients need & guidance with their home buying & selling for the right price under the best terms. 
            Jenessa dedicates her time & efforts to delivering clients white-glove service & strives for best results. 
            She earns the respect of her clients by working diligently on their behalf & by always offering them candid advice with warm & 
            friendly approach.  Moreover, she will make sure to perform the highest levels of honesty, integrity & professionalism with gratitude.
        </p>
        <a href="/about/" class="btn btn-dark">Read more about Jenessa</a>
    </div>
</div>