<?php
/**
 * The template for displaying custom search results for listings
 *
 *
 * @package Jenessa Seruela Real Estate
 * @subpackage JenessaSeruelaRealEstate
 * @since 1.0
 * @version 1.0
 */

get_header(); 

    if($_GET['s'] && !empty($_GET['s'])) {
        $keyword = $_GET['s'];
    }

    $args = [ 
        'post_type'         =>  'listings',
        'posts_per_page'    =>  -1,
        's'                 =>  $keyword
    ];
    $properties = new WP_Query( $args );

?>
<header>
    <div class="header-image-post" style="background-image: url('<?php echo get_template_directory_uri() ?>/img/bg-1600x867.jpg');">
        <div class="header-caption container text-center d-none d-md-block">
        </div>
    </div>	
</header>
<div id="properties-page" class="card-deck">
    <h1 class="text-center">PROPERTIES</h1>
    <p class="text-center text-muted">
        <form role="search" method="get" id="searchform" class="search-form" action="<?php echo home_url( '/listings/' ); ?>">
            <label> 
                <input type="search" class="search-field form-control" name="s" id="s" <?php if(is_search()) { ?>value="<?php the_search_query(); ?>" placeholder="Enter Province, City or Project" <?php } else { ?>value="Enter keywords &hellip;" placeholder="Enter Province, City or Project" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;"<?php } ?> />
            </label>
            <input type="submit" id="searchsubmit" class="search-submit btn btn-dark" value="Search" />
        </form>
    </p>
    <div class="search-result mt-4 mb-4 text-center" style="width: 100%;">
        <?php if ( $properties->have_posts() && !empty($keyword) ) : ?>
            <h6 class="page-title"><?php printf( __( 'Search Results for: %s', 'jenseruela' ), '<span>"' . get_search_query() . '"</span>' ); ?></h6>
        <?php else : ?>
            <h6 class="page-title">Nothing Found</h6>
            <p>Sorry, but nothing matched your search terms. Please try again with some different keywords.</p>
        <?php endif; ?>
    </div>

<div class="row mt-5 mb-5">
		<?php
			/* Start the Loop */
			while ( $properties->have_posts() && !empty($keyword) ) : $properties->the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				// get_template_part( 'template-parts/post/content', 'excerpt' );
            ?>    
                <div class="listing col-sm-4 col-pad-2">
                    <div class="card">
                        <img class="card-img-top" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
                        <div class="card-body">
                            <h6 class="card-title"><?php the_title(); ?></h6>
                            <p>
                                <?php echo get_post_meta( get_the_ID(), 'location', true); ?><br/>
                                <b><?php echo get_post_meta( get_the_ID(), 'price', true); ?></b>
                            </p>
                            <div class="text-center">
                                <a class="btn btn-dark" href="<?php the_permalink(); ?>">View Details</a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
			endwhile; // End of the loop.

			the_posts_pagination( array(
				'screen_reader_text' =>	 ' ',
				'prev_text' => '<span class="screen-reader-text">' . __( 'Previous page', 'jenseruela' ) . '</span>',
				'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'jenseruela' ) . '</span>',
				'before_page_number' => '<span class="meta-nav screen-reader-text"></span>',
			) );

        /* Restore original Post Data */
        wp_reset_postdata();
		?>

</div> <!-- end of .row -->
    <?php 
        // Display Contact Us Section.
        get_template_part( 'template-parts/section/section', 'contact' );
    ?>
</div> <!-- end of #properties-page -->
	
<?php get_footer(); ?>