<?php
/**
 * Template for displaying search forms in jenseruela
 *
 * @package Jenessa Seruela Real Estate
 * @subpackage JenessaSeruelaRealEstate
 * @since 1.0
 * @version 1.0
 */

?>

<?php $unique_id = esc_attr( uniqid( 'search-form-' ) ); ?>

<!-- <form role="search" method="get" class="searchform" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">

	<input type="search" id="<?php //echo $unique_id; ?>" class="form-control searchfield" placeholder="<?php //echo esc_attr_x( 'Search for...', 'placeholder', 'jenseruela' ); ?>" value="<?php //echo get_search_query(); ?>" name="s" />
	<button type="submit" id="searchsubmit" class="searchsubmit"><span class="screen-reader-text"><?php //echo _x( 'Search', 'submit button', 'jenseruela' ); ?></span></button>
</form> -->

<form role="search" method="get" id="<?php echo $unique_id; ?>" class="search-form" action="<?php echo home_url( '/listings/' ); ?>">
    <label>  
        <input type="search" id="<?php echo $unique_id; ?>" class="form-control search-field" placeholder="<?php echo esc_attr_x( 'Enter Province, City or Project', 'placeholder', 'jenseruela' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
    </label>
    <input type="submit" id="searchsubmit" class="search-submit btn btn-dark" value="Search" />
</form>